# C TLS/TCP server #

A TLS/TCP server that uses [libreSSL](https://www.libressl.org/) and [libevent](https://libevent.org/) to listen on and react to events on multiple non-blocking sockets simultanously. It runs in a single thread/process.

This project is an enhancement of the [libevent-tcp-server](https://bitbucket.org/kallevedin/libevent-tcp-server) that obviously adds TLS support.

### Operating systems ###
* OpenBSD
* (Linux)

It requires **libbsd** to compile under Linux, and fiddling back and forth with includes and linking. If this was a serious project I guess one should use automake or something, as different linux distros put the includes and libs in different directories. (Tried it on OpenSUSE and Arch Lunix.)

### License ###
[Creative Commons Zero](https://creativecommons.org/share-your-work/public-domain/cc0). This essentially means that you can do whatever you want with the code, including copying it and claiming that you wrote it. However, if you do that, various timestamps will show that I wrote it first :P

### Authors ###
* Kalle Vedin

