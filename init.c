// init.c

#include <stdlib.h>
#include <stdio.h>
#include <tls.h>

#include <unistd.h>

#ifdef __linux__
    // for strtonum
#include <limits.h>
#include <bsd/stdlib.h>
#endif

#include "misc.h"
#include "server.h"

struct tls_config *configure_server_tls(char *ca_file_path, char *serv_cert_path, char *serv_priv_key_path, char *serv_priv_key_pass) {
    struct tls_config *cfg = NULL;
    uint8_t *mem = NULL;
    size_t mem_len;

    if (tls_init()) 
        err(1, "failed to initalize TLS");

    if (!(cfg = tls_config_new()))
        err(1, NULL);

    if (!(mem = tls_load_file(ca_file_path, &mem_len, NULL)))
        err(1, "TLS load CA");
    if (tls_config_set_ca_mem(cfg, mem, mem_len))
        err(1, "TLS set CA memory");

    if (!(mem = tls_load_file(serv_cert_path, &mem_len, NULL)))
        err(1, "TLS load server cert");
    if (tls_config_set_cert_mem(cfg, mem, mem_len))
        err(1, "TLS set server cert memory");

    if (!(mem = tls_load_file(serv_priv_key_path, &mem_len, serv_priv_key_pass)))
        err(1, "TLS load server private key");
    if (tls_config_set_key_mem(cfg, mem, mem_len))
        err(1, "TLS set server private key memory");

    tls_config_prefer_ciphers_server(cfg);
    return cfg;
}

int main(int argc, char** argv) {
    struct tls_config *t;
    const char* err_str;
    int listen_port = 1919;
    char *ca_file_path, *cert_file_path, *privkey_file_path;
    char *cwd;

#ifdef DEBUG
    setbuf(stdout, NULL);
#endif

    if (!(cwd = malloc(MAX_PATH_LEN)))
        err(1, NULL);
    if (!getcwd(cwd, MAX_PATH_LEN))
        err(1, NULL);

    if (argc == 2) {
        listen_port = strtonum(argv[1], 1, 65335, &err_str);
        if (err_str != NULL) {
            printf("Port must be in the interval 1-65335. \"%s\" is: %s\n", argv[1], err_str);
            exit(1);
        }
    }

    ca_file_path = join_str(cwd, "/cipher/ca.pem");
    cert_file_path = join_str(cwd, "/cipher/server.pem");
    privkey_file_path = join_str(cwd, "/cipher/server.key");
    LOG("reading certificate files\n"); 
    LOG("\tCA file: %s\n", ca_file_path);
    LOG("\tcert file: %s\n", cert_file_path);
    LOG("\tprivkey file: %s\n", privkey_file_path);

    t = configure_server_tls(ca_file_path, cert_file_path, privkey_file_path, "test-server-pass"); // TODO: fix password
    DEBUG_LOG("Successfully read cert/key files.\n");

#ifdef __OpenBSD__
    if (unveil("/", ""))
        err(1, "unveil /");
    if (pledge("stdio inet", ""))
        err(1, NULL);
#endif
    free(cwd);
    free(ca_file_path);
    free(cert_file_path);
    free(privkey_file_path);

    DEBUG_LOG("starting server\n");
    start_server(listen_port, t);
}

