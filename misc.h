// misc.h

#include <err.h>
#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#define MAX_PATH_LEN 1024
#define CLIENT_TIMEOUT_SEC 120 
#define MAX_SOCKETS 255
#define MAX_CLIENTS MAX_SOCKETS - 1
#define BUF_SIZE 1024

//#define DEBUG

// returns a NEW string that the caller should free()
char *join_str(char *s1, char *s2);

void LOG(char *format, ...);
void DEBUG_LOG(char *format, ...);


