#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include "misc.h"

// returns a pointer to a NEW string that should be free()d by the caller
char *join_str(char *c1, char *c2) {
    char *joined;
    if (!(joined = malloc(strlen(c1) + strlen(c2))))
        err(1, NULL);
    joined[0] = '\0';
    strcat(joined, c1);
    strcat(joined, c2);
    return joined;
}

void LOG(char *format, ...) {
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);
}

void DEBUG_LOG(char *format, ...) {
#ifdef DEBUG
    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);
#endif
}

