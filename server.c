#include <event.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/types.h>
#include <tls.h>
#include <unistd.h>

#include "misc.h"
#include "server.h"
#include "client.h"



int server_sock_fd;
int server_port;
struct tls_config *server_tls_config = NULL;
struct tls *server_tls = NULL;
static struct sockaddr_in server_sock_addr;
static struct event_base *event_base;
static struct event serv_sock_event;
static struct timeval one_second = { 1, 0 };

void start_server(int listen_port, struct tls_config *tls);
static int create_server_sock(int port); 
static void handle_signal(int signum, short what, void *arg);
static void every_second(int unknown, short what, void *arg); 
static void server_socket_event(int sock, short what, void *arg);
static int create_server_sock(int port);
 


void start_server(int listen_port, struct tls_config *tls_conf) {
    struct event *sigint_event;
    struct event *sighup_event;
    struct event *every_second_event;

    server_port = listen_port;
    server_tls_config = tls_conf;

    // create TLS entry
    if (NULL == (server_tls = tls_server()))
        err(1, "tls_server");
    if (tls_configure(server_tls, server_tls_config))
        err(1, "tls_configure");

    // setup event handling
    if (!(event_base = event_init()))
        err(1, "failed to initalize libevent");

    if (!(sigint_event = malloc(sizeof(struct event))))
        err(1, NULL);
    if (!(sighup_event = malloc(sizeof(struct event))))
        err(1, NULL);
    if (!(every_second_event = malloc(sizeof(struct event))))
        err(1, NULL);

    signal_set(sigint_event, SIGINT, &handle_signal, sigint_event);
    signal_add(sigint_event, NULL);
    signal_set(sighup_event, SIGHUP, &handle_signal, sighup_event);
    signal_add(sighup_event, NULL);

    evtimer_set(every_second_event, &every_second, every_second_event);
    evtimer_add(every_second_event, &one_second);

    // setup socket for accepting clients
	server_sock_fd = create_server_sock(server_port);
    LOG("server listening at port %d\n", server_port);

    event_set(&serv_sock_event, server_sock_fd, EV_PERSIST|EV_READ, &server_socket_event, NULL);
    event_add(&serv_sock_event, NULL);
    DEBUG_LOG("watching server sock fd=%d for read-events\n", server_sock_fd);

    event_dispatch();
    LOG("libevent exited");
}

// libevent makes this safe, we dont have to care about the obscureness that applies
// to normal raw signal handlers.
void handle_signal(int signum, short what, void *arg) {
    if (signum == SIGINT) {
        DEBUG_LOG("SIGINT\n");
        exit(0);
    } else if (signum == SIGHUP) {
        DEBUG_LOG("SIGHUP\n");
    } else {
        DEBUG_LOG("Unknown signal: %d\n", signum);
        exit(1);
    }
}

// don't trust that this is EXACTLY one second interval
static void every_second(int unknown, short what, void *arg) {
    struct event *e = (struct event *)arg;
    evtimer_add(e, &one_second);

    client_once_per_sec();
}

// handle clients connecting
static void server_socket_event(int sock_fd, short what, void *arg) {
    int client_fd;
    struct sockaddr_in *addr;
    socklen_t addr_len = sizeof(struct sockaddr_in);

    if (!(addr = malloc(sizeof(struct sockaddr_in))))
        err(1, NULL);

    DEBUG_LOG("incomming client connection\n");
    if ((client_fd = accept(sock_fd, (struct sockaddr *)addr, &addr_len)) == -1)
        err(1, NULL);

    client_new(client_fd, addr);
}

// create non-blocking listening sock
// this is used for accepting incomming connections
static int create_server_sock(int port) {
    int sock_fd;
    int enable = 1;

    memset(&server_sock_addr, 0, sizeof(struct sockaddr_in));
    server_sock_addr.sin_family = AF_INET;
    server_sock_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_sock_addr.sin_port = htons(port);

    if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        err(1, "failed to open sock");

    if (setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, (int *) &enable, sizeof(int)) < 0)
        err(1, "failed to set SO_REUSEADDR");

    if (ioctl(sock_fd, FIONBIO, (int *) &enable) < 0)
        err(1, "failed to make server socket non-blocking");    

    if (bind(sock_fd, (struct sockaddr *) &server_sock_addr, sizeof(server_sock_addr)))
        err(1, "failed to bind server socket");

    if (listen(sock_fd, 10) == -1)
        err(1, "failed to listen");

    return sock_fd;
}

