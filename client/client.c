// client.c

#include <arpa/inet.h>
#include <err.h>
#include <event.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/queue.h>
#include <sys/socket.h>
#include <tls.h>
#include <unistd.h>

#include "../misc.h"

#define BUF_SIZE 1024

struct timeval so_often = { 3, 0 }; // 3 sec

struct event_base *event_base;
struct event *event_every_so_often;
struct event *event_socket;
struct event *event_stdin;

int sock_fd;
short socket_what_copy;

struct tls_config *cfg = NULL;
struct tls *ctx = NULL;

struct write_buf_entry {
    char *buf;
    ssize_t len;
    ssize_t written;
    SIMPLEQ_ENTRY(write_buf_entry) entry;
};

SIMPLEQ_HEAD(write_buffer_head, write_buf_entry) write_buffer;



void update_socket_event(short what); 
void every_so_often(int unknown, short what, void *arg);
void handle_socket_read();
void handle_write_buffer(); 
void handle_socket_event(int fd, short what, void *arg);
void handle_stdin_event(int fd, short what, void *arg);
void setup_tls(const char *ca_pem_file_path);
struct sockaddr *resolve_sockaddr(const char *hostname, const char *port, socklen_t *len);
struct timeval last_write;

 

void update_socket_event(short what) {
    struct event *e = event_socket;

    if (socket_what_copy == what) {
        return;
    }
    event_del(e);
    event_set(e, sock_fd, what, &handle_socket_event, NULL);
    event_add(e, NULL);
    socket_what_copy = what;
}

void every_so_often(int unknown, short what, void *arg) {
    struct timeval now;
    char *ping = "PING\n";

    evtimer_add(event_every_so_often, &so_often);

    LOG(".");

    if (gettimeofday(&now, NULL))
        err(1, NULL);

    time_t sec_since_last_write = now.tv_sec - last_write.tv_sec;
    if (sec_since_last_write > 60) {
        struct write_buf_entry *e;
        ssize_t ping_len = strlen(ping);

        if (!(e = malloc(sizeof(struct write_buf_entry))))
            err(1, NULL);
        if (!(e->buf = malloc(ping_len)))
            err(1, NULL);
        memcpy(e->buf, ping, ping_len);
        e->len = ping_len;
        e->written = 0;
        SIMPLEQ_INSERT_HEAD(&write_buffer, e, entry);

        memcpy(&last_write, &now, sizeof(struct timeval));
    }
}

void handle_socket_read() {
    ssize_t size;
    char buf[BUF_SIZE+1];

    size = tls_read(ctx, buf, BUF_SIZE);

    if (size == -1) {
        LOG("TLS read error: %s\n", tls_error(ctx));
        exit(1);
    } else if (size == TLS_WANT_POLLIN) {
        DEBUG_LOG("read: TLS_WANT_POLLIN\n");
        update_socket_event(socket_what_copy | EV_READ);
        return;
    } else if (size == TLS_WANT_POLLOUT) {
        DEBUG_LOG("read: TLS_WANT_POLLOUT\n");
        update_socket_event(socket_what_copy | EV_WRITE);
    } else if (size == 0) {
        LOG("disconnected\n");
        exit(0);
    } else if (size > 0) {
        buf[size+1] = '\0';
        if (buf[size] == '\n')
            buf[size] = '\0';

        LOG("%s\n", buf);
    } else {
        LOG("UNKNOWN EVENT\n");
        exit(1);
    }
}

void handle_write_buffer() {
    ssize_t size;
    struct write_buf_entry *e;
    char *data_ptr;
    ssize_t len;

    e = SIMPLEQ_FIRST(&write_buffer);
    if (e == NULL) {
        update_socket_event(socket_what_copy & (~EV_WRITE));
    }

    data_ptr = (char *)(e->buf + e->written);
    len = e->len - e->written;

    size = tls_write(ctx, data_ptr, len);

    if (size == -1) {
        LOG("TLS write error: %s\n", tls_error(ctx));
        exit(1);
    } else if (size == TLS_WANT_POLLIN) {
        DEBUG_LOG("write: TLS_WANT_POLLIN\n");
        update_socket_event(socket_what_copy | EV_READ);
        return;
    } else if (size == TLS_WANT_POLLOUT) {
        DEBUG_LOG("write: TLS_WANT_POLLOUT\n");
        update_socket_event(socket_what_copy | EV_WRITE);
        return;
    } else if (size == 0) {
        DEBUG_LOG("wrote 0 bytes?\n");
        return;
    } else if (size > 0) {
        DEBUG_LOG("wrote %ld bytes\n", size);
        e->written += size;
        if (e->written == e->len) {
            DEBUG_LOG("done with this buffer segment\n");
            SIMPLEQ_REMOVE_HEAD(&write_buffer, entry);
        }
    } else {
        LOG("UNKNOWN EVENT\n");
        exit(1);
    }

    if (SIMPLEQ_EMPTY(&write_buffer)) {
        DEBUG_LOG("write buffer is empty\n");
        update_socket_event(socket_what_copy & (~EV_WRITE));
    }
    DEBUG_LOG("write ret\n");
}

void handle_socket_event(int fd, short what, void *arg) {
    if (what & EV_READ) {
        DEBUG_LOG("handle socket read event\n");
        handle_socket_read();
    } 
    if (what & EV_WRITE) {
        DEBUG_LOG("handle socket write event\n");
        handle_write_buffer();
    }
}

void handle_stdin_event(int fd, short what, void *arg) {
    struct write_buf_entry *e;
    ssize_t len;
    struct timeval now;

    if (!(e = malloc(sizeof(struct write_buf_entry))))
        err(1, NULL);
    if (!(e->buf = malloc(BUF_SIZE)))
        err(1, NULL);

    if ((len = read(fd, e->buf, BUF_SIZE)) < 0)
        err(1, NULL);
    else if (len == 0) {
        // user EOF
        if (tls_close(ctx))
            DEBUG_LOG("failed to terminate TLS session\n");
        if (close(sock_fd))
            DEBUG_LOG("failed to close socket\n");
        exit(0);
    }

    if (gettimeofday(&now, NULL))
        err(1, NULL);

    e->len = len;
    e->written = 0;
    SIMPLEQ_INSERT_TAIL(&write_buffer, e, entry);
    memcpy(&last_write, &now, sizeof(struct timeval));
    update_socket_event(socket_what_copy | EV_WRITE);
}


void setup_tls(const char *ca_pem_file_path) {
    if (tls_init() != 0)
        err(1, "tls_init");
    if (!(cfg = tls_config_new()))
        err(1, "tls_config_new");
    if (ca_pem_file_path != NULL) {
        if (tls_config_set_ca_file(cfg, ca_pem_file_path))
            err(1, "tls_config_set_ca_file");
    }
    if (!(ctx = tls_client()))
        err(1, "tls_client");
    if (tls_configure(ctx, cfg))
        err(1, "tls_configure: %s", tls_error(ctx));
}

// returns a NEW struct sockaddr that the caller have to free()
struct sockaddr *resolve_sockaddr(const char *hostname, const char *port, socklen_t *len) {
    struct addrinfo hints, *res;
    int ret;

    memset(&hints, 0, sizeof(struct addrinfo));

    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = IPPROTO_TCP;
    hints.ai_flags = AI_ADDRCONFIG | AI_NUMERICSERV;

    if ((ret = getaddrinfo(hostname, port, &hints, &res))) {
        DEBUG_LOG("failed to lookup %s: %s\n", hostname, gai_strerror(ret));
        return NULL;
    }

    struct sockaddr *result = NULL;
    if (!(result = malloc(res->ai_addrlen)))
        err(1, "malloc res->ai_addrlen=%u bytes", res->ai_addrlen);

    if (!(memcpy(result, res->ai_addr, res->ai_addrlen)))
        err(1, NULL);

    len = &res->ai_addrlen;
    freeaddrinfo(res);
    return result;
}

int main(int argc, char *argv[])
{
    struct sockaddr *serv_addr;
    socklen_t serv_addr_len;
    unsigned short server_port;

    char *host_name = "localhost.test_dummy.com";
    char *host_port = "1919";
    const char *err_str = NULL;
    const char *tls_version = NULL;
    const char *tls_cipher_suit = NULL;

    SIMPLEQ_INIT(&write_buffer);

#ifdef DEBUG
    setbuf(stdout, NULL);
#endif

#ifdef __OpenBSD__
    if (unveil("/", "r"))
        err(1, NULL);
#endif

    server_port = strtonum(host_port, 1, 65335, &err_str);
    if (err_str != NULL) {
        LOG("Server port has to be in the interval 1-65335.\n");
        exit(1);
    }

    if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
        err(1, NULL);

    setup_tls("../cipher/ca.pem");

    if (!(serv_addr = resolve_sockaddr(host_name, host_port, &serv_addr_len)))
        err(1, "failed to resolve %s port %s", host_name, host_port);

    if (connect(sock_fd, (const struct sockaddr *)serv_addr, sizeof(struct sockaddr_in)))
        err(1, "failed to connect to server");

    if (tls_connect_socket(ctx, sock_fd, host_name))
        err(1, "tls_connect error: %s", tls_error(ctx));

    if (tls_handshake(ctx))
        err(1, "TLS handshake error: %s\n", tls_error(ctx));

    if (!tls_peer_cert_provided(ctx)) {
        LOG("Peer provided no TLS certificate.\n");
        exit(1);
    }

    LOG("Connected to %s\n", host_name);
    if (!(tls_version = tls_conn_version(ctx)))
        err(1, "TLS error: %s", tls_error(ctx));
    if (!(tls_cipher_suit = tls_conn_cipher(ctx)))
        err(1, "TLS error: %s", tls_error(ctx));

    LOG("Cipher used: %s %s\n", tls_version, tls_cipher_suit);

    int enable = 1;
    if (ioctl(sock_fd, FIONBIO, (int *) &enable) < 0)
        err(1, "failed to make client socket non-blocking");

#ifdef __OpenBSD__
    if (pledge("stdio inet unveil", ""))
        err(1, "pledge: stdio inet unveil");
#endif

    if (!(event_base = event_init()))
        err(1, "failed to initialize libevent");
    if (!(event_every_so_often = malloc(sizeof(struct event))))
        err(1, NULL);
    if (!(event_socket = malloc(sizeof(struct event))))
        err(1, NULL);
    if (!(event_stdin = malloc(sizeof(struct event))))
        err(1, NULL);

#ifdef __OpenBSD__
    if (unveil("/", ""))
        err(1, "unveil /");
    if (pledge("stdio inet", ""))
        err(1, "pledge: stdio inet");
#endif

    evtimer_set(event_every_so_often, &every_so_often, NULL);
    evtimer_add(event_every_so_often, &so_often);

    socket_what_copy = EV_READ|EV_PERSIST; 
    event_set(event_socket, sock_fd, socket_what_copy, &handle_socket_event, NULL);
    event_add(event_socket, NULL);

    event_set(event_stdin, 0, EV_READ|EV_PERSIST, &handle_stdin_event, NULL);
    event_add(event_stdin, NULL);

    event_dispatch();
    exit(0);
}
