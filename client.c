// client.c

#include <event.h>
#include <netinet/tcp.h>
#include <stdlib.h>
#include <sys/socket.h>

#ifdef __linux__
#include <bsd/err.h>
#include <bsd/sys/tree.h>
#endif
#ifdef __OpenBSD__
#include <err.h>
#include <sys/tree.h>
#endif

#include <unistd.h>

#include "client.h"
#include "misc.h"
#include "server.h"



char* banner = "\x1b[7m\x1b[31mHERP DERP\x1b[0m\n\x1b[7m\x1b[31mDERP HERP\x1b[0m\n\n";
long long next_client_id = 0;

struct client_node {
    RB_ENTRY(client_node) entry;
    int fd;
    struct client *c;
};

int client_node_cmp(struct client_node *e1, struct client_node *e2);
void client_new(int fd, struct sockaddr_in *addr);
int client_destroy(struct client *client);
void client_handle_event(int fd, short what, void *client);
void client_once_per_sec();
struct client_node *get_client_node(int fd); 
struct client *get_client(int fd);
void client_write_buffer(struct client *c, char *data, ssize_t len, enum segment_flags flags);
void client_write(struct client *c, char *data, ssize_t len);
void client_write_static(struct client *c, char *data, ssize_t len);
void update_event(struct client *client, short what);
void read_from_client(struct client *c);
void write_client_buffer(struct client *c);

// rb-tree containing all clients, and their associated data
RB_HEAD(client_tree, client_node) client_tree_head = RB_INITIALIZER(&client_tree_head);
RB_PROTOTYPE(client_tree, client_node, entry, client_node_cmp)
RB_GENERATE(client_tree, client_node, entry, client_node_cmp)

int client_node_cmp(struct client_node *cn1, struct client_node *cn2) {
    int a = cn1->fd;
    int b = cn2->fd;

    if (a > b) return 1;
    else if (a < b) return -1;
    return 0;
}



void client_new(int fd, struct sockaddr_in *addr) {
    int on = 1;
    struct client_node *cn;
    struct client *c;
    struct tls *cctx;
    const char *err_msg;

    DEBUG_LOG("creating client for fd:%d\n", fd);

    if (NULL != get_client_node(fd))
        err(1, "file descriptor %d re-used!", fd);

    if (tls_accept_socket(server_tls, &cctx, fd)) {
        if (NULL != (err_msg = tls_error(cctx))) {
            LOG("failed to accept client (fd:%d) because: %s\n", err_msg);
            return;
        } else {
            LOG("tls_accept_socket() failed for unknown reason!\n");
            err(1, NULL);
        }
    }

    if (!(cn = malloc(sizeof(struct client_node))))
        err(1, NULL);
    if (!(c = malloc(sizeof(struct client))))
        err(1, NULL);

    cn->c = c;
    cn->fd = fd;

    c->id = next_client_id++;
    c->fd = fd;
    c->addr = addr;
    c->state = NEW_CONNECTION;
    c->event = malloc(sizeof(struct event));
    c->what_copy = EV_READ|EV_WRITE|EV_PERSIST;
    c->write_buf_len = 0;
    c->ctx = cctx;

    if (gettimeofday(&c->last_read_timestamp, NULL))
        err(1, NULL);

    SIMPLEQ_INIT(&c->write_buf_head);
    RB_INSERT(client_tree, &client_tree_head, cn);
    setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, (void *)&on, sizeof(int));
    event_set(c->event, c->fd, c->what_copy, &client_handle_event, c);
    event_add(c->event, NULL);

    client_write_static(c, banner, strlen(banner));
    return;
}

int client_destroy(struct client *c) {
    struct buf_entry *be;

    DEBUG_LOG("terminating client: %d\n", c->fd);
    event_del(c->event);
    while (!SIMPLEQ_EMPTY(&c->write_buf_head)) {
        be = SIMPLEQ_FIRST(&c->write_buf_head);
        SIMPLEQ_REMOVE_HEAD(&c->write_buf_head, entry);
        if (be->flags & SHOULD_FREE)
            free(be->segment);
        free(be);
    }
    RB_REMOVE(client_tree, &client_tree_head, get_client_node(c->fd));

    if (tls_close(c->ctx))
        DEBUG_LOG("failed to close TLS connection with client (id:%lld, fd:%d): %s\n", c->id, c->fd, tls_error(c->ctx));

    tls_free(c->ctx);
    if (close(c->fd))
        err(1, NULL);

    free(c);
    return 0;
}

void client_handle_event(int fd, short what, void *arg) {
    struct client *c = arg;

    if (what & EV_READ)
        read_from_client(c);
    if (what & EV_WRITE)
        write_client_buffer(c);

#ifdef DEBUG
    if (!(what & (EV_READ | EV_WRITE))) {
        DEBUG_LOG("client_handle_event() received unknown event");
    }
#endif
}

// is called about once per second
void client_once_per_sec() {
    struct client_node *cn;
    struct timeval now;
    gettimeofday(&now, NULL);

    time_t now_sec = now.tv_sec;
    RB_FOREACH(cn, client_tree, &client_tree_head) {
        struct client *c = cn->c;
        time_t c_sec = c->last_read_timestamp.tv_sec;
        time_t sec_since_last_read = now_sec - c_sec;

        if (sec_since_last_read > CLIENT_TIMEOUT_SEC) {
            LOG("client timeout (id:%lld, fd:%d)\n", c->id, c->fd);
            client_destroy(c);
        }
    }
}

struct client_node *get_client_node(int fd) {
    struct client_node find;
    find.fd = fd;

    return RB_FIND(client_tree, &client_tree_head, &find);
}

// returns the client with this fd, or NULL
struct client *get_client(int fd) {
    struct client_node *cn = get_client_node(fd);
    return cn ? cn->c : NULL;
}

void client_write_buffer(struct client *c, char *data, ssize_t len, enum segment_flags flags) {
    struct buf_entry *b;
    if (!(b = malloc(sizeof(struct buf_entry))))
        err(1, NULL);

    b->segment_len = len;
    b->written = 0;
    b->segment = data;
    b->flags = flags;
    c->write_buf_len += len; // total len

    SIMPLEQ_INSERT_TAIL(&c->write_buf_head, b, entry);
}

// data will be free()d after it has been written
void client_write(struct client *c, char *data, ssize_t len) {
    client_write_buffer(c, data, len, SHOULD_FREE);
    update_event(c, c->what_copy|EV_WRITE);
}

// data will NOT be free()d after it has been written
void client_write_static(struct client *c, char *data, ssize_t len) {
    client_write_buffer(c, data, len, DONT_FREE);
    update_event(c, c->what_copy|EV_WRITE);
}

void update_event(struct client *c, short what) {
    if (c->what_copy == what) {
        return;
    }

    DEBUG_LOG("updating event for client (id:%lld, fd:%d)\n", c->id, c->fd);

    event_del(c->event);
    event_set(c->event, c->fd, what, &client_handle_event, c);
    event_add(c->event, NULL);

    c->what_copy = what;
}

void read_from_client(struct client *c) {
    struct client_node *cn;
    ssize_t size;
    char *buf;

    DEBUG_LOG("read event for client (id:%lld, fd:%d)\n", c->id, c->fd);
    if (!(buf = malloc(BUF_SIZE)))
        err(1, NULL);

#ifdef DEBUG
    memset(buf, 0, BUF_SIZE);
#endif

    DEBUG_LOG("\tTLS read\n"); 
    size = tls_read(c->ctx, buf, BUF_SIZE);
    DEBUG_LOG("\tTLS read returned: %zd\n", size);
    if (size == -1) {
        DEBUG_LOG("TLS error for client (id:%lld, fd:%d): %s\n", c->id, c->fd, tls_error(c->ctx));
        client_destroy(c);
    } else if (size == TLS_WANT_POLLIN) {
        DEBUG_LOG("\tTLS_WANT_POLLIN\n");
        update_event(c, c->what_copy | EV_READ);
        free(buf);
        return;
    } else if (size == TLS_WANT_POLLOUT) {
        DEBUG_LOG("\tTLS_WANT_POLLOUT\n");
        update_event(c, c->what_copy | EV_WRITE);
        free(buf);
        return;
    }
 
    if (!size) {
        DEBUG_LOG("client disconnected (id:%lld, fd:%d)\n", c->id, c->fd);
        client_destroy(c);
        free(buf);
        return;
    }

    if (gettimeofday(&c->last_read_timestamp, NULL) == -1)
        err(1, NULL);

#ifdef DEBUG
    if (buf[size] == '\n')
        buf[size] = '\0';
    DEBUG_LOG("\tdistributing chat message: %s\n", buf);
#endif

    RB_FOREACH(cn, client_tree, &client_tree_head) {
        if (cn->c->fd != c->fd) {
            char *c_buf = malloc(size);
            memcpy(c_buf, buf, size);
            client_write(cn->c, c_buf, size);
        }
    }
    free(buf);
}

// writes data from the client write buffer
void write_client_buffer(struct client *c) {
    ssize_t size;

    DEBUG_LOG("write event for client (id:%lld, fd:%d)\n", c->id, c->fd);

    if (c->write_buf_len != 0) {
        // empty out the client buffer
        // write one message (buffer segment) at a time...
        // if not all data could be written, then advance the offset pointer (b->written)
        struct buf_entry *b = SIMPLEQ_FIRST(&c->write_buf_head);
        void *buf_ptr = (void *)((char *)b->segment + b->written);
        ssize_t remaining_len = b->segment_len - b->written;

        DEBUG_LOG("\twrite-buffer is %zd bytes, current segment is %zd bytes\n", c->write_buf_len, remaining_len);

        size = tls_write(c->ctx, buf_ptr, remaining_len);
        if (size == -1) {
            LOG("TLS error for client (id:%lld, fd:%d): %s", c->id, c->fd, tls_error(c->ctx));
            client_destroy(c);
            return;
        } else if (size == TLS_WANT_POLLIN) {
            DEBUG_LOG("client (id:%lld, fd:%d): TLS_WANTS_POLLIN\n", c->id, c->fd);
            update_event(c, c->what_copy | EV_READ); 
        } else if (size == TLS_WANT_POLLOUT) {
            DEBUG_LOG("client (id:%lld, fd:%d): TLS_WANTS_POLLOUT\n", c->id, c->fd);
            update_event(c, c->what_copy | EV_WRITE);
        } else {
            DEBUG_LOG("client (id:%lld, fd:%d): successfully sent some data to this client\n", c->id, c->fd);
            b->written += size;
            c->write_buf_len -= size;

            if (b->written > b->segment_len || c->write_buf_len < 0) {
                DEBUG_LOG("emptying client buffer entry failed!!! wrote past end!? size=%zd, b->written=%zd\n", size, b->written);
                DEBUG_LOG("    c->write_buf_len=%zd\n", c->write_buf_len);
                exit(1);
            } else if (b->written < b->segment_len) {
                return;
            }

            SIMPLEQ_REMOVE_HEAD(&c->write_buf_head, entry);
            if (b->flags & SHOULD_FREE)
                free(b->segment);
            free(b);
        }
    }
    if (!c->write_buf_len)
        update_event(c, c->what_copy & (~EV_WRITE));
}


