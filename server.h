// server.h
#include <tls.h>

extern int server_socket_fd;
extern int server_port;

extern struct tls_config *server_tls_config;
extern struct tls *server_tls;

void start_server(int port, struct tls_config *tls);

